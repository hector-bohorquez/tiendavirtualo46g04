package com.mintic.ciclo3.backCliente.controlador;

import com.mintic.ciclo3.backCliente.modelo.Cliente;
import com.mintic.ciclo3.backCliente.servicio.ClienteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/clientes")
public class ClienteController {
    
    @Autowired
    private ClienteServicio clienteServicio;

    // Listar los registros de la Tabla
    @GetMapping("/list")
    public List<Cliente> consultarTodo() {
        return (clienteServicio.getClientes());
    }

    // Buscar por Id
    @GetMapping("/list/{id}")
    public Cliente buscarPorId(@PathVariable Integer id) {
        return clienteServicio.getCliente(id);
    }

    // Crear o insertar un registro
    @PostMapping("/")
    public ResponseEntity<Cliente> agregar(@RequestBody Cliente cliente) {
        Cliente obj = clienteServicio.grabarCliente(cliente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Actualización de un registro
    @PutMapping("/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente) {
        Cliente obj = clienteServicio.getCliente(cliente.getIdcliente());
        if (obj != null) {
            obj.setDireccion(cliente.getDireccion());
            obj.setApellidos(cliente.getApellidos());
            obj.setDocumento(cliente.getDocumento());
            obj.setEmail(cliente.getEmail());
            obj.setNombres(cliente.getNombres());
            obj.setTipdoc(cliente.getTipdoc());
            clienteServicio.grabarCliente(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Elimina un registro 
    @DeleteMapping("/{id}")
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id) {
    	Cliente obj = clienteServicio.getCliente(id);
    	if(obj != null) {
        	clienteServicio.delete(id);
    	}else {
    		return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    

}
