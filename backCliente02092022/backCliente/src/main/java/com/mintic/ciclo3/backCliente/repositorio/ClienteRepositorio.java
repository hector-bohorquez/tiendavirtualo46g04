
package com.mintic.ciclo3.backCliente.repositorio;

import com.mintic.ciclo3.backCliente.modelo.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClienteRepositorio extends JpaRepository<Cliente, Integer>{
    
}
