package com.mintic.ciclo3.backCliente.servicio;

import com.mintic.ciclo3.backCliente.modelo.Cliente;
import com.mintic.ciclo3.backCliente.repositorio.ClienteRepositorio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ClienteServicio implements IClienteServicio{

	@Autowired
	private ClienteRepositorio clienteRepo;

        //Listar los registros de la tabla (Read)
	@Override
	public List<Cliente> getClientes() {
		return clienteRepo.findAll();
	}

        //Buscar el Cliente por el id
	@Override
	public Cliente getCliente(Integer id) {
		return clienteRepo.findById(id).orElse(null);
	}

        //Crear un registro en la tabla (insert into..) Create y Update
	@Override
	public Cliente grabarCliente(Cliente cliente) {
		return clienteRepo.save(cliente);
	}

        //Elimina un registro Delete
	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		clienteRepo.deleteById(id);
	}
    
}
