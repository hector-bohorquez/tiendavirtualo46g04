package com.mintic.ciclo3.backCliente.servicio;

import com.mintic.ciclo3.backCliente.modelo.Cliente;
import java.util.List;

public interface IClienteServicio {
    public List<Cliente> getClientes();

    public Cliente getCliente(Integer id);

    public Cliente grabarCliente(Cliente cliente);

    public void delete(Integer id);
    
}
